const express = require ('express')
const router = express.Router();

//This allows us to use all the routes defined in taskRoute.js
const taskRoute = require('.routes/taskRoute')';'


//router to get all the tasks
//this route expects to recieve a GET request at the URL "/tasks"
//The whole URL is at "https:"localhost:3001/tasks"this is because of the "/tasks"

router.get('/', (req, res)=>{

  taskController.getAllTasks();

});




//Route to create a new task
//This route expects to recieave a PORT request at the URL "/tasks"
//The whole URL is at "https://localhost:3001/tasks"

router.post('/', (req, res)=>{

	taskController.addNewTasks();

});

//Use module.exports to export the router object so we can use it in index.js
module.exports = router;